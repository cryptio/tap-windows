# TAP-Windows
A fork of OpenVPN's TAP-Windows

## Build
Requirements:
- Visual Studio 2019
- [WDK 7.1](https://www.microsoft.com/en-ca/download/details.aspx?id=11800)

Build instructions:
[https://community.openvpn.net/openvpn/wiki/BuildingTapWindows](https://community.openvpn.net/openvpn/wiki/BuildingTapWindows)

*Note*: In order for the tap-windows installer to work on systems, it must be signed with a Software Publisher Certificate (SPC). These must be purchased, they cannot be obtained for free. If the software is not signed with one, users will get the following error code:

    devcon.exe returned 2
    
This is because unsigned device drivers cannot be installed by default. Even if you permit this activity, OpenVPN will not be able to acquire a TAP handle.
